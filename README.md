# Bitbucket Repository Push Hook
# v.13052016 (0.0.1)
### See README.md
###### **Author:** João Oliveira
###### **Email:** oyamok at gmail dot com 

### Description
> Used this to deploy my LARAVEL 5 applications on my VPN


### Features:
  - Bitbucket.org webhook.
  - Support multiple repositories.
  - Fetch and clone repositories.
  - Automatic folder structure.
  - Support post hook command execution.

### Requirements
  - PHP 7.0+
  - GIT
  - Shell Access
  - PHP Functions (**system, exec, shell_exec**)
  - SSH (With **empty** passphrase) **For Bitbucket**

### Tips
> Add "COMPOSER_HOME" environment variable to Apache 2 "envvars" file, if you will use composer as post hook command.
> 
> Check "/var/www" permissions and groups, some issues may happen if you got the wrong ones.

### Installation
Upload script to your "/var/www/your_domain_folder"  
Bitbucket -> Repository Administration -> Webhooks -> Add 
```sh 
http://<domain>/<path>/hook.php
```

### Development
Want to contribute? Great!  
I will never ask for donations, but if you are happy and want to contribute, you are free to do so.

### Todo
 - Fixes
