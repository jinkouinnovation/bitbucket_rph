<?php

/*
 * Bitbucket Repository Push Hook
 * v.13052016 (0.0.1)
 *
 * See README.md
 *
 * João Oliveira
 * mailto: oyamok at gmail dot com
 */

$LOG_FILE = 'log.log';
$LOG_ENABLED = false;

function xLOG_CLEAR()
{
	global $LOG_FILE;

	if (!empty($GLOBALS['LOG_ENABLED'])) {
		if (is_file($LOG_FILE)) {
			unlink($LOG_FILE);
		}
	}
}

function xLOG($msg)
{
	if (!empty($GLOBALS['LOG_ENABLED'])) {
		$datetime = date('Y.m.d H:i:s');
		file_put_contents($GLOBALS['LOG_FILE'], $datetime . " -> " . $msg . "\n", FILE_APPEND | LOCK_EX);
		flush();
	}
}

function xLOG_DEBUG($name, $var)
{
	xLOG($name . ': ' . print_r($var, true));
}

function xLOG_ERROR($msg)
{
	xLOG('ERROR: ' . $msg);
}