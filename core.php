<?php

/*
 * Bitbucket Repository Push Hook
 * v.13052016 (0.0.1)
 *
 * See README.md
 *
 * João Oliveira
 * mailto: oyamok at gmail dot com
 */

define('DEFAULT_FOLDER_MODE', 0777);

$PAYLOAD = [];
$REPOSITORY = '';
$BRANCHES = [];

/*
 * initialize log
 */
function initLog()
{
	global $CFG, $LOG_ENABLED, $LOG_FILE;

	if (!empty($CFG['log_enable'])) {
		$LOG_ENABLED = true;
	}
	if (!empty($CFG['log_file'])) {
		$LOG_FILE = $CFG['log_file'];
	}
	if (!empty($CFG['log_reset'])) {
		xLOG_CLEAR();
	}

}

/*
 * initialize bitbucket payload
 */
function initPayload()
{
	global $PAYLOAD;

	xLOG('Bitbucket Repository Push Hook');
	xLOG('v.13052016 (0.0.1)');
	xLOG($_SERVER['HTTP_X_EVENT_KEY']);
	xLOG($_SERVER['HTTP_X_HOOK_UUID']);
	xLOG($_SERVER['HTTP_USER_AGENT']);
	xLOG('Remote: ' . $_SERVER['REMOTE_ADDR']);

	if (isset($_POST['payload'])) {
		$PAYLOAD = $_POST['payload'];
	} else {
		$PAYLOAD = json_decode(file_get_contents('php://input'));
	}

	if (empty($PAYLOAD)) {
		xLOG_ERROR("Payload was not received");
		exit;
	}

	if (!isset($PAYLOAD->repository->name, $PAYLOAD->push->changes)) {
		xLOG_ERROR("Invalid payload data was received!");
		exit;
	}

	$dd = print_r($PAYLOAD, true);
	file_put_contents('debug_payload.txt', print_r($dd, true));

	xLOG("Valid payload was received");

}

/*
 * fetch payload
 */
function fetchPayload()
{
	global $REPOSITORY, $PAYLOAD, $PROJECTS, $BRANCHES;

	// get repository name
	$REPOSITORY = $PAYLOAD->repository->name;
	if (empty($PROJECTS[$REPOSITORY])) {
		xLOG_ERROR("Not found repository config for '$REPOSITORY'!");
		exit;
	}

	foreach ($PAYLOAD->push->changes as $change) {
		if (is_object($change->new) && $change->new->type == "branch" && isset($PROJECTS[$REPOSITORY][$change->new->name])) {
			// create branch name for checkout
			array_push($BRANCHES, $change->new->name);
			xLOG("Changes in branch '" . $change->new->name . "' was fetched");
		}
	}

	if (empty($BRANCHES)) {
		xLOG("Nothing to update");
	}

}

/*
 * checks deploy
 */
function checksDeploy()
{
	global $REPOSITORY, $CFG, $PROJECTS, $BRANCHES;

	// check for repositories folder
	if (!is_dir($CFG['repositories'])) {
		$mode = (!empty($CFG['folder_mode'])) ? $CFG['folder_mode'] : DEFAULT_FOLDER_MODE;
		if (mkdir($CFG['repositories'], $mode, true)) {
			xLOG("Creating repository folder '" . $CFG['repositories'] . " (" . decoct($mode) . ") for '$REPOSITORY'");
		} else {
			xLOG_ERROR("Error creating repository folder '" . $CFG['repositories'] . " for '$REPOSITORY'! Exiting.");
			exit;
		}
	}

	// create folder for each pushed branch
	foreach ($BRANCHES as $branch) {
		if (!is_dir($PROJECTS[$REPOSITORY][$branch]['deploy_path'])) {
			$mode = (!empty($CFG['folder_mode'])) ? $CFG['folder_mode'] : DEFAULT_FOLDER_MODE;
			if (mkdir($PROJECTS[$REPOSITORY][$branch]['deploy_path'], $mode, true)) {
				xLOG("Creating project folder '" . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . " (" . decoct($mode) . ") for '$REPOSITORY' branch '$branch'");
			} else {
				xLOG_ERROR("Error creating project folder '" . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . " for '$REPOSITORY' branch '$branch'! Exiting.");
				exit;
			}
		}
	}

}

/*
 * log debug information
 */
function logDebug()
{
	global $REPOSITORY, $CFG, $BRANCHES;

	if ($CFG['log_debug']) {
		xLOG_DEBUG('CFG', $CFG);
		xLOG_DEBUG('REPOSITORY', $REPOSITORY);
		xLOG_DEBUG('PATH', $CFG['repositories'] . '/' . $REPOSITORY . '.git/');
		xLOG_DEBUG('BRANCHES', $BRANCHES);
	}
}
/*
 * fetch repository
 */
function fetchRepository()
{
	global $REPOSITORY, $CFG;

	// compose current repository path
	$repositoryPath = $CFG['repositories'] . '/' . $REPOSITORY . '.git/';
	$absProjectPath = str_replace('\\', '/', __DIR__) . '/';
	xLOG(getcwd());

	// clone full repository
	if (!is_dir($repositoryPath) || !is_file($repositoryPath . 'HEAD')) {
		xLOG("Cloning repository for '$REPOSITORY'");
		xLOG('cd ' . $CFG['repositories'] . ' && ' . $CFG['git_cmd'] . ' clone --mirror git@bitbucket.org:' . $CFG['owner'] . '/' . $REPOSITORY . '.git');
		$code = system('cd ' . $CFG['repositories'] . ' && ' . $CFG['git_cmd'] . ' clone --mirror git@bitbucket.org:' . $CFG['owner'] . '/' . $REPOSITORY . '.git', $status);
		xLOG_DEBUG('Code', $code);
		xLOG_DEBUG('Status', $status);
		if ($status !== 0) {
			xLOG_ERROR('Cannot clone repository git@bitbucket.org:' . $CFG['owner'] . '/' . $REPOSITORY . '.git');
			exit;
		}
	} // fetch changes
	else {
		xLOG("Fetching repository '$REPOSITORY'");
		xLOG('cd ' . $repositoryPath . ' && ' . $CFG['git_cmd'] . ' fetch');
		$code = system('cd ' . $repositoryPath . ' && ' . $CFG['git_cmd'] . ' fetch', $status);
		xLOG_DEBUG('Code', $code);
		xLOG_DEBUG('Status', $status);
		if ($status !== 0) {
			xLOG_ERROR("Cannot fetch repository '$REPOSITORY' in '$repositoryPath'!");
			exit;
		}
	}

}

/*
 * deploy repository
 */
function deployRepository()
{
	global $REPOSITORY, $CFG, $PROJECTS, $BRANCHES;

	// compose current repository path
	$repositoryPath = $CFG['repositories'] . '/' . $REPOSITORY . '.git/';
	$absProjectPath = str_replace('\\', '/', __DIR__) . '/';
	xLOG(getcwd());

	// checkout
	foreach ($BRANCHES as $branch) {
		xLOG('cd ' . $repositoryPath . ' && GIT_WORK_TREE=' . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . ' ' . $CFG['git_cmd'] . ' checkout -f ' . $branch);
		$code = system('cd ' . $repositoryPath . ' && GIT_WORK_TREE=' . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . ' ' . $CFG['git_cmd'] . ' checkout -f ' . $branch, $status);
		xLOG_DEBUG('Code', $code);
		xLOG_DEBUG('Status', $status);
		if ($status !== 0) {
			xLOG_ERROR("Cannot checkout branch '$branch' in repository '$REPOSITORY'!");
			exit;
		}

		xLOG(getcwd());
		if (!empty($PROJECTS[$REPOSITORY][$branch]['post_hook_cmd'])) {
			xLOG('cd ' . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . ' && ' . $PROJECTS[$REPOSITORY][$branch]['post_hook_cmd']);
			$code = system('cd ' . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . ' && ' . $PROJECTS[$REPOSITORY][$branch]['post_hook_cmd'], $status);
			xLOG_DEBUG('Code', $code);
			xLOG_DEBUG('Status', $status);
			if ($status !== 0) {
				xLOG_ERROR("Error in post hook command for branch '$branch' in repository '$REPOSITORY'!");
				//exit;
			}
		}

		// log deployment
		$hash = rtrim(shell_exec('cd ' . $repositoryPath . ' && ' . $CFG['git_cmd'] . ' rev-parse --short ' . $branch));

		xLOG("Branch '$branch' was deployed in '" . $PROJECTS[$REPOSITORY][$branch]['deploy_path'] . "', commit #$hash");
	}
}