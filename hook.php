<?php

/*
 * Bitbucket Repository Push Hook
 * v.13052016 (0.0.1)
 *
 * See README.md
 *
 * João Oliveira
 * mailto: oyamok at gmail dot com
 */

// initialize
require_once('log.php');
require_once('core.php');

// load configuration
require_once('config.php');

// deploy
initLog();
initPayload();
fetchPayload();
checksDeploy();
logDebug();
fetchRepository();
deployRepository();