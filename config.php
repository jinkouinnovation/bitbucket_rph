<?php

/*
 * Bitbucket Repository Push Hook
 * v.13052016 (0.0.1)
 *
 * See README.md
 *
 * João Oliveira
 * mailto: oyamok at gmail dot com
 */

$REPOSITORIES_PATH = 'repositories';
$PROJECTS_PATH = '/var/www';

/*
 * configuration
 */
$CFG = [
	'owner'        => 'bitbucket_account', // bitbucket.org team name/username *REQUIRED*
	'git_cmd'      => '/usr/bin/git', // git command *REQUIRED*
	'repositories' => $REPOSITORIES_PATH, // folder containing all repositories *REQUIRED*
	'folder_mode'  => 0777, // folder mode *OPTIONAL*
	'log_enable'   => true, // enable log *OPTIONAL*
	'log_reset'    => true, // reset log each time *OPTIONAL*
	'log_debug'    => true, // show debug information in log *OPTIONAL*
	'log_file'     => 'log.log', // log file name *OPTIONAL*
];

/*
 * projects
 */
$PROJECTS = [
	// key is a bitbucket.org repository name
	'repository_name_1'  => [
		'branch_name' => [ // branch name
			'deploy_path'   => $PROJECTS_PATH . 'deploy_path', // project deploy path *REQUIRED*
			'post_hook_cmd' => 'composer update', // command to execute after deploy, optional
		],
	],

	// key is a bitbucket.org repository name
	'repository_name_2' => [
		'branch_name' => [ // branch name
			'deploy_path'   => $PROJECTS_PATH . 'deploy_path', // project deploy path *REQUIRED*
			'post_hook_cmd' => 'composer update', // command to execute after deploy *OPTIONAL*
		],
	],
];